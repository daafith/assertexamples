package speedcamera;

/**
 * Entry point for BDD assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
public class BddAssertions {

  /**
   * Creates a new instance of <code>{@link speedcamera.MotorCopAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.MotorCopAssert then(speedcamera.MotorCop actual) {
    return new speedcamera.MotorCopAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link speedcamera.SpeedCameraAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.SpeedCameraAssert then(speedcamera.SpeedCamera actual) {
    return new speedcamera.SpeedCameraAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link speedcamera.VehicleAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.VehicleAssert then(speedcamera.Vehicle actual) {
    return new speedcamera.VehicleAssert(actual);
  }

  /**
   * Creates a new <code>{@link BddAssertions}</code>.
   */
  protected BddAssertions() {
    // empty
  }
}
