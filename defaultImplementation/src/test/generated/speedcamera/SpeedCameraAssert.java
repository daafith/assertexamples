package speedcamera;

import org.assertj.core.api.AbstractAssert;

/**
 * {@link SpeedCamera} specific assertions - Generated by CustomAssertionGenerator.
 */
public class SpeedCameraAssert extends AbstractAssert<SpeedCameraAssert, SpeedCamera> {

  /**
   * Creates a new <code>{@link SpeedCameraAssert}</code> to make assertions on actual SpeedCamera.
   * @param actual the SpeedCamera we want to make assertions on.
   */
  public SpeedCameraAssert(SpeedCamera actual) {
    super(actual, SpeedCameraAssert.class);
  }

  /**
   * An entry point for SpeedCameraAssert to follow AssertJ standard <code>assertThat()</code> statements.<br>
   * With a static import, one can write directly: <code>assertThat(mySpeedCamera)</code> and get specific assertion with code completion.
   * @param actual the SpeedCamera we want to make assertions on.
   * @return a new <code>{@link SpeedCameraAssert}</code>
   */
  public static SpeedCameraAssert assertThat(SpeedCamera actual) {
    return new SpeedCameraAssert(actual);
  }

  /**
   * Verifies that the actual SpeedCamera's correctedSpeed is equal to the given one.
   * @param correctedSpeed the given correctedSpeed to compare the actual SpeedCamera's correctedSpeed to.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera's correctedSpeed is not equal to the given one.
   */
  public SpeedCameraAssert hasCorrectedSpeed(int correctedSpeed) {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // overrides the default error message with a more explicit one
    String assertjErrorMessage = "\nExpected correctedSpeed of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";
    
    // check
    int actualCorrectedSpeed = actual.getCorrectedSpeed();
    if (actualCorrectedSpeed != correctedSpeed) {
      failWithMessage(assertjErrorMessage, actual, correctedSpeed, actualCorrectedSpeed);
    }

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual SpeedCamera is licenseRevoked.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera is not licenseRevoked.
   */
  public SpeedCameraAssert isLicenseRevoked() {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // check
    if (!actual.isLicenseRevoked()) {
      failWithMessage("\nExpected actual SpeedCamera to be licenseRevoked but was not.");
    }
    
    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual SpeedCamera is not licenseRevoked.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera is licenseRevoked.
   */
  public SpeedCameraAssert isNotLicenseRevoked() {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // check
    if (actual.isLicenseRevoked()) {
      failWithMessage("\nExpected actual SpeedCamera not to be licenseRevoked but was.");
    }
    
    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual SpeedCamera's measuredSpeed is equal to the given one.
   * @param measuredSpeed the given measuredSpeed to compare the actual SpeedCamera's measuredSpeed to.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera's measuredSpeed is not equal to the given one.
   */
  public SpeedCameraAssert hasMeasuredSpeed(int measuredSpeed) {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // overrides the default error message with a more explicit one
    String assertjErrorMessage = "\nExpected measuredSpeed of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";
    
    // check
    int actualMeasuredSpeed = actual.getMeasuredSpeed();
    if (actualMeasuredSpeed != measuredSpeed) {
      failWithMessage(assertjErrorMessage, actual, measuredSpeed, actualMeasuredSpeed);
    }

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual SpeedCamera is pictureTaken.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera is not pictureTaken.
   */
  public SpeedCameraAssert isPictureTaken() {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // check
    if (!actual.isPictureTaken()) {
      failWithMessage("\nExpected actual SpeedCamera to be pictureTaken but was not.");
    }
    
    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual SpeedCamera is not pictureTaken.
   * @return this assertion object.
   * @throws AssertionError - if the actual SpeedCamera is pictureTaken.
   */
  public SpeedCameraAssert isNotPictureTaken() {
    // check that actual SpeedCamera we want to make assertions on is not null.
    isNotNull();

    // check
    if (actual.isPictureTaken()) {
      failWithMessage("\nExpected actual SpeedCamera not to be pictureTaken but was.");
    }
    
    // return the current assertion for method chaining
    return this;
  }

}
