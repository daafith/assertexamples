package speedcamera;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
public class Assertions {

  /**
   * Creates a new instance of <code>{@link speedcamera.MotorCopAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.MotorCopAssert assertThat(speedcamera.MotorCop actual) {
    return new speedcamera.MotorCopAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link speedcamera.SpeedCameraAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.SpeedCameraAssert assertThat(speedcamera.SpeedCamera actual) {
    return new speedcamera.SpeedCameraAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link speedcamera.VehicleAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static speedcamera.VehicleAssert assertThat(speedcamera.Vehicle actual) {
    return new speedcamera.VehicleAssert(actual);
  }

  /**
   * Creates a new <code>{@link Assertions}</code>.
   */
  protected Assertions() {
    // empty
  }
}
