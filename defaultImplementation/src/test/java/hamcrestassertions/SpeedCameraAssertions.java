package hamcrestassertions;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import speedcamera.SpeedCamera;

public class SpeedCameraAssertions {
	
	public static Matcher<SpeedCamera> hasCorrectedSpeedTo(final int correctedSpeed) {
		return new TypeSafeMatcher<SpeedCamera>() {
		      
		      public void describeTo(final Description description) {
		         description.appendText("The camera should have corrected the speed to ").appendValue(correctedSpeed);
		      }
		      
		      public void describeMismatchSafely(final SpeedCamera camera, final Description mismatchDescription) {
			         mismatchDescription.appendText(" alas, the camera corrected it to ").appendValue(camera.getCorrectedSpeed());
			  }
		      
		      public boolean matchesSafely(final SpeedCamera camera) {
			         return correctedSpeed == camera.getCorrectedSpeed();
			  }
		};
	}
	
	public static Matcher<SpeedCamera> hasTakenAPicture(final boolean hasTakenPicture) {
		return new TypeSafeMatcher<SpeedCamera>() {
		      
		      public void describeTo(final Description description) {
		         description.appendText("Taking a picture should have returned ").appendValue(hasTakenPicture);
		      }
		      
		      public void describeMismatchSafely(final SpeedCamera item, final Description mismatchDescription) {
		         mismatchDescription.appendText(" returned ").appendValue(item.isPictureTaken());
		      }

		      public boolean matchesSafely(final SpeedCamera camera) {
		         return hasTakenPicture == camera.isPictureTaken();
		      }
		      
		};
	}
	
	public static Matcher<SpeedCamera> hasRevokedLicense(final boolean hasRevokedLicense) {
		return new TypeSafeMatcher<SpeedCamera>() {
		      
		      public void describeTo(final Description description) {
		         description.appendText("Revoking licensed should have returned ").appendValue(hasRevokedLicense);
		      }
		      
		      public void describeMismatchSafely(final SpeedCamera item, final Description mismatchDescription) {
		         mismatchDescription.appendText(" returned ").appendValue(item.isPictureTaken());
		      }

		      public boolean matchesSafely(final SpeedCamera camera) {
		         return hasRevokedLicense == camera.isPictureTaken();
		      }
		      
		};
	}

	
}
