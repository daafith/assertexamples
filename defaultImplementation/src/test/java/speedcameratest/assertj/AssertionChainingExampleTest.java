package speedcameratest.assertj;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import speedcamera.JUnitSoftAssertions;
import speedcamera.SpeedCamera;
import speedcamera.Vehicle;

public class AssertionChainingExampleTest {

	private Vehicle vehicle;
	private SpeedCamera speedCamera;
	
	@Rule
	public final JUnitSoftAssertions softly = new JUnitSoftAssertions();
	
	@Before
	public void setUp() {
		vehicle = new Vehicle(0);
		speedCamera = new SpeedCamera();
		vehicle.addObserver(speedCamera);
	}
	
	@Test
	public void oneTestToRuleThemAll() {
		vehicle.passSpeedCamera(104);
		softly.assertThat(vehicle).as("Cruising speed").hasSpeed(104);
		softly.assertThat(speedCamera).as("Measured speed").hasMeasuredSpeed(104);
		softly.assertThat(speedCamera).as("Corrected speed").hasCorrectedSpeed(99);
		softly.assertThat(speedCamera).as("Picture moment?").isPictureTaken();
		softly.assertThat(speedCamera).as("License revoked?").isNotLicenseRevoked();
	}
	
	@Test
	public void noneShallPass() {
		vehicle.passSpeedCamera(1000);
		softly.assertThat(vehicle).as("Cruising speed").hasSpeed(2);
		softly.assertThat(speedCamera).as("Measured speed").hasMeasuredSpeed(3);
		softly.assertThat(speedCamera).as("Corrected speed").hasCorrectedSpeed(4);
		softly.assertThat(speedCamera).as("Picture moment?").isNotLicenseRevoked();
		softly.assertThat(speedCamera).as("License revoked?").isNotLicenseRevoked();
	}
	
}
