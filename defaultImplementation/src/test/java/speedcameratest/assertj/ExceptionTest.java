package speedcameratest.assertj;

import static org.assertj.core.api.Assertions.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;

import speedcamera.Vehicle;

public class ExceptionTest {
	
	private final static Logger LOG = Logger.getLogger(Exception.class.getName());
	private Vehicle vehicle;
	
	@Test
	public void expectIllegalArgumentConstructor() {
		try { 
			vehicle = new Vehicle(-1);
			fail("A negative speed is impossible");
		} catch (IllegalArgumentException iaex) {
			LOG.log(Level.INFO, "As expected, the vehicle constructor threw ", iaex);
		}
	}
	
	@Test
	public void expectIllegalArgumentPassByCamera() {
		vehicle = new Vehicle(0);
		try { 
			vehicle.passSpeedCamera(-1);
			fail("A negative speed is impossible");
		} catch (IllegalArgumentException iaex) {
			LOG.log(Level.INFO, "As expected, vehicle's passSpeedCamera threw ", iaex);
		}
	}

}
