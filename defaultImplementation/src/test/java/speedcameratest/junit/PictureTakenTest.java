package speedcameratest.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import speedcamera.SpeedCamera;
import speedcamera.Vehicle;

/**
 * @author davidbaak<br />
 * The purpose of this is test is to show you a pretty straightforward approach in JUnit.<br />
 * Specs of Camera's picture trigger are:<br />
 * <b>when</b> correctedSpeed >= 51 <b>then</b> camera should take picture<br />
 */

public class PictureTakenTest {
	
	private Vehicle vehicle;
	private SpeedCamera speedCamera;
	
	@Before
	public void setUp() {
		vehicle = new Vehicle(0);
		speedCamera = new SpeedCamera();
		vehicle.addObserver(speedCamera);
	}
	
	@Test
	public void expectNoTicketOneBelowSpeedLimit() {
		vehicle.passSpeedCamera(53);
		assertFalse(speedCamera.isPictureTaken());
	}
	
	@Test
	public void expectNoTicketExactSpeedLimit() {
		vehicle.passSpeedCamera(54);
		assertFalse(speedCamera.isPictureTaken());
	}
	
	@Test
	public void expectATicketOneOverSpeedLimit() {
		vehicle.passSpeedCamera(55);
		assertEquals(51, speedCamera.getCorrectedSpeed());
		assertTrue(speedCamera.isPictureTaken());
	}
	
}
