package speedcameratest.junit;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import speedcamera.SpeedCamera;
import speedcamera.Vehicle;

/**
 * @author davidbaak<br />
 * The purpose of this is test is to show you a pretty straightforward approach in JUnit.<br />
 * Specs of Camera's speed correction are:<br />
 * <ul>
 * <li>30 <= (correction=3) <= 50</li>
 * <li>51 <= (correction=4) <= 100</li>
 * <li>101 <= (correction=5)</li>
 * </ul>
 */

public class SpeedCorrectionTest {
	
	private Vehicle vehicle;
	private SpeedCamera speedCamera;
	
	@Before
	public void setUp() {
		vehicle = new Vehicle(0);
		speedCamera = new SpeedCamera();
		vehicle.addObserver(speedCamera);
	}
	
	@Test
	public void expectNoCorrectionWhenOneBelowMeausureTrigger() {
		vehicle.passSpeedCamera(29);
		assertEquals(29, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfThreeWhenMeausureTrigger() {
		vehicle.passSpeedCamera(30);
		assertEquals(27, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfThreeWhenOneAboveMeausureTrigger() {
		vehicle.passSpeedCamera(31);
		assertEquals(28, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfThreeWhenOneBelowSpeedLimit() {
		vehicle.passSpeedCamera(49);
		assertEquals(46, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfThreeWhenSpeedLimit() {
		vehicle.passSpeedCamera(50);
		assertEquals(47, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfFourWhenOneOverSpeedLimit() {
		vehicle.passSpeedCamera(51);
		assertEquals(47, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfFourWhenOneBelowRevocationLimit() {
		vehicle.passSpeedCamera(99);
		assertEquals(95, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfFourWhenRevocationLimit() {
		vehicle.passSpeedCamera(100);
		assertEquals(96, speedCamera.getCorrectedSpeed());
	}
	
	@Test
	public void expectCorrectionOfFiveWhenRevocationLimit() {
		vehicle.passSpeedCamera(101);
		assertEquals(96, speedCamera.getCorrectedSpeed());
	}
	
	
	@After
	public void printCorrectedSpeed() {
		System.out.println(speedCamera.getCorrectedSpeed());
	}

}
