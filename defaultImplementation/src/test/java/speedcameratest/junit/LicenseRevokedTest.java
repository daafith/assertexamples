package speedcameratest.junit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import speedcamera.SpeedCamera;
import speedcamera.Vehicle;

/**
 * @author davidbaak<br />
 * The purpose of this is test is to show you a pretty straightforward approach in JUnit.<br />
 * Specs of Camera's speed correction are:<br />
 * <b>when</b> correctedSpeed >= speedLimit+50 <b>then/<b> license should be revoked<br />
 */

public class LicenseRevokedTest {
	
	private Vehicle vehicle;
	private SpeedCamera speedCamera;
	
	@Before
	public void setUp() {
		vehicle = new Vehicle(0);
		speedCamera = new SpeedCamera();
		vehicle.addObserver(speedCamera);
	}	

	@Test
	public void expectNoLicenseRevocationOneBelowRevocationLimit() {
		vehicle.passSpeedCamera(104);
		assertFalse(speedCamera.isLicenseRevoked());
	}
	
	@Test
	public void expectLicenseRevocationExactRevocationLimit() {
		vehicle.passSpeedCamera(105);
		assertTrue(speedCamera.isLicenseRevoked());
	}
	
	@Test
	public void expectLicenseRevocationOneOverRevocationLimit() {
		vehicle.passSpeedCamera(106);
		assertTrue(speedCamera.isLicenseRevoked());
	}
	
}
