package speedcameratest.hamcrest;

import static hamcrestassertions.SpeedCameraAssertions.hasCorrectedSpeedTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import speedcamera.SpeedCamera;
import speedcamera.Vehicle;

/**
 * @author davidbaak<br />
 * The purpose of this is test is to show you a custom approach with Hamcrest.<br />
 * Specs of Camera's speed correction are:<br />
 * <ul>
 * <li>30 >= (correction=3) <= 50</li>
 * <li>51 <= (correction=4) <= 100</li>
 * <li>101 <= (correction=5)</li>
 * </ul>
 */

public class SpeedCorrectionTest {
	
	private Vehicle vehicle;
	private SpeedCamera speedCamera;
	
	@Before
	public void setUp() {
		vehicle = new Vehicle(0);
		speedCamera = new SpeedCamera();
		vehicle.addObserver(speedCamera);
	}
	
	@Test
	public void expectNoCorrectionWhenOneBelowMeausureTrigger() {
		vehicle.passSpeedCamera(29);
		assertThat(speedCamera, hasCorrectedSpeedTo(29));
	}
	
}
