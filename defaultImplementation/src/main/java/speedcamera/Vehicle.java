package speedcamera;

import java.util.Observable;

public class Vehicle extends Observable {
	
	private int speed;
	
	public Vehicle(int speed) {
		setSpeed(speed);
	}
	
	private void setSpeed(int speed) {
		if (speed < 0) {
			throw new IllegalArgumentException(speed + " is not a valid speed");
		}
		this.speed = speed;
	}
  
	public void passSpeedCamera(int speed) {
		setSpeed(speed);
		setChanged();
		notifyObservers(speed);
	}
	
	public int getSpeed() {
		return speed;
	}

}
