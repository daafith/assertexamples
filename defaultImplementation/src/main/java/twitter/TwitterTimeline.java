package twitter;


interface TwitterTimeline {

	void showGoodNewsOnTimeLine(String goodNews);

	void showBadNewsOnTimeLine(String badNews);
	
}
