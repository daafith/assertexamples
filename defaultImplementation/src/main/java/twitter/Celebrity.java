package twitter;

import java.util.ArrayList;
import java.util.List;

public class Celebrity {

	List<TwitterTimeline> listeners = new ArrayList<TwitterTimeline>();

    public void addListener(TwitterTimeline toAdd) {
        listeners.add(toAdd);
    }

    public void tweetGoodNews(String goodNews) {

        for (TwitterTimeline listener : listeners) {
        	listener.showGoodNewsOnTimeLine(goodNews);
    	}
    }
    
    public void tweetBadNews(String badNews) {

        for (TwitterTimeline listener : listeners) {
        	listener.showBadNewsOnTimeLine(badNews);
    	}
    }
}
