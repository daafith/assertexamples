package twitter;

import java.util.ArrayList;
import java.util.List;

public class Fan implements TwitterTimeline {
	
	private String currentMood;
	private int numberOfTweets;
	private List<String> listOfTweets;
	
	public Fan() {
		listOfTweets = new ArrayList<String>();
		changeMood(MoodSwings.DEFAULT.toString());
		tweet(currentMood);
	}
	
	public String getCurrentMood() {
		return currentMood;
	}
	
	public List<String> getListOfTweets() {
		return listOfTweets;
	}
	
	public int getNumberOfTweets() {
		return numberOfTweets;
	}
	
	public void showGoodNewsOnTimeLine(String goodNews) {
		changeMood(MoodSwings.EXCITED.toString());
		retweetNews(goodNews);
	}

	public void showBadNewsOnTimeLine(String badNews) {
		changeMood(MoodSwings.SAD.toString());
		retweetNews(badNews);
	}
	
	private void changeMood(String changedMood) {
		if (!currentMood.equals(changedMood)) {
			currentMood = changedMood;
		} else {
			tweet(currentMood);
		}
	}
	
	private void retweetNews(String retweet) {
		listOfTweets.add("RT: " + retweet);
		updateNumberOfTweets(listOfTweets);
	}
	
	private void tweet(String sameMood) {
		listOfTweets.add(sameMood);
		updateNumberOfTweets(listOfTweets);
	}
	
	private void updateNumberOfTweets(List<String> updatedList) {
		numberOfTweets = updatedList.size();
	}
	
	private enum MoodSwings {
		DEFAULT("I am #bored and jonesing for some news."),
		EXCITED("Excited! I'll tweet this and look #interesting."),
		SAD("Noooooo! Let me tweet my #sadness.");
	
		private final String moodState;
		
		private MoodSwings(String moodState) {
				this.moodState = moodState;
		}

		public String toString() {
			return moodState;
		}
	};
	
}
