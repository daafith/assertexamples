package twitterplus;

import java.util.ArrayList;
import java.util.List;

abstract class Person {
	
	protected String myName;
	protected TwitterTimeLine myTimeline = null;
	protected int myTimeLineId = 0;
	protected boolean isTimeLineCreated = false;
	protected boolean isLoggedIn = false;
	protected List<TwitterService> listeners = new ArrayList<TwitterService>();
	
	protected Person(String name) {
		myName = name;
	}
	
	protected String getName() {
		return myName;
	}
	
	protected Person createTwitterAccount(String twitterName) {
		if (!isTimeLineCreated) {
			myTimeline = new TwitterTimeLine("@"+twitterName);
			isTimeLineCreated = true;
		}
		return this;
	}

	protected String getTwitterName() {
		if (isTimeLineCreated) {
			return myTimeline.getAccountName();
		} else {
			return "Sorry " + myName + ", you have no twitteraccount";
		}
	}
	
	protected Person login() {
		if (isTimeLineCreated && !isLoggedIn) {
			myTimeLineId = listeners.size();
			listeners.add(myTimeline);
			isLoggedIn = true;
		} else {
			System.out.println("Are you sure you have an account? Or are you already logged in?");
		}
		return this;
	}
	
	protected Person readTimeline() {
		if (isTimeLineCreated && isLoggedIn) {
			myTimeline.readTimeline();
		} else {
			System.out.println("Sorry " + myName + ", you have no twitteraccount");
		}
		return this;
	}
	
	protected abstract Person publishTweet(String tweet);


}
