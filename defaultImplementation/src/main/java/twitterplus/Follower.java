package twitterplus;

import org.joda.time.DateTime;

abstract class Follower extends Person {

	protected Follower(String name) {
		super(name);
	}
	
	@Override
	public Follower publishTweet(String tweet) {
		if(isLoggedIn) {
			String dt = new DateTime().toString();
			for (int i = 0; i <= listeners.size(); i ++) {
				if (i == myTimeLineId) {
					listeners.get(i).updateTimeLine(myTimeline.getAccountName(), dt, tweet);
				}   	
			}
		}
		return this;
	}

}
