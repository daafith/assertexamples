package twitterplus;

import org.joda.time.DateTime;


public class Celebrity extends Person {
	
	public Celebrity(String name) {
		super(name);
	}
	
	public Celebrity addFollower(Follower follower) {
		listeners.add(follower.myTimeline);
		return this;
	}


	@Override
	public Celebrity publishTweet(String tweet) {
		String dt = new DateTime().toString();
		for (TwitterService listener : listeners) { 
			listener.updateTimeLine(myTimeline.getAccountName(), dt, tweet);   	
		}
		return this;
	}
		
	
//	public void publishTweet(Tweet tweet) {
//		for (int i = 0; i <= listeners.size(); i ++) {
//			if (i != myTimeLineId) {
//				listeners.get(i).updateTimeLine(tweet);
//			}   	
//		}
//	}


}
