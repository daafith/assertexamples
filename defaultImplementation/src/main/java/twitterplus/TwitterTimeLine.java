package twitterplus;

import java.util.ArrayList;
import java.util.List;

public class TwitterTimeLine implements TwitterService {
	
	private String accountName;
	private List<String[]> tweets = new ArrayList<String[]>();
	
	public TwitterTimeLine(String accountName) {
		this.accountName = accountName;
	}
	
	public String getAccountName() {
		return accountName;
	}
	
	public int showNumberOfTweets() {
		return tweets.size();
	}

	public void updateTimeLine(String author, String time, String tweet) {
		String[] entry  = new String[3];
		entry[0] = author;
		entry[1] = time;
		entry[2] = tweet;
		tweets.add(entry);
	}
	
	public void readTimeline() {
		for (String[] tweet : tweets) {		
			System.out.println(tweet[0] + " " + tweet[1]);
			System.out.println(tweet[2]);
		}
		System.out.println(System.lineSeparator());
	}

}
