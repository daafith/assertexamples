package twitterplus;


public class Main {

	public static void main(String[] args) {
		
		Celebrity celebTwo = new Celebrity("Jaapie");
		celebTwo.createTwitterAccount("Yoyo");
		celebTwo.login();
		celebTwo.publishTweet("Hello World");
		celebTwo.publishTweet("42");
		
		Paparazzo pap = new Paparazzo("photo freak");
		pap.createTwitterAccount("click_click");
		pap.login();
		celebTwo.addFollower(pap);

		pap.publishTweet("click");
		celebTwo.readTimeline();
		celebTwo.publishTweet("14");
		celebTwo.readTimeline();
		pap.readTimeline();
		
		Fan fan = new Fan("Joe");
		fan
			.createTwitterAccount("celebrity_fan")
			.login();
		celebTwo
			.addFollower(fan)
			.publishTweet("Welcome fan!");
		fan
			.publishTweet("I am following @Yoyo!")
			.readTimeline();

	}

}
