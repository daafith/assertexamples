# Assertion style examples
Some rudimentary examples of test assertion styles. Main code has some implementations of the observer pattern. 
### JUnit
The default framework for [unit testing](http://junit.org ).
```java
	assertTrue(foo.isBar());
	assertEquals("foo", bar.getFoo());
```
	
### Hamcrest
A customisable [assertion](https://github.com/hamcrest) library.
```java
	assertThat(foo.isBar(), is(true));
	assertThat(bar.getFoo(), equalTo("foo"));
```
	
### AssertJ
A customisable [assertion](http://joel-costigliola.github.io/assertj/index.html) library.

### Observer pattern
A shortened example in Java using the java.util.Observable and the java.util.Observer.
```java
	public class ObjectBeingObserved extends Observable {
		//implementation omitted
		
		public void doSomething(Foo somethingNoteworthy){
			setChanged();
			notifyObservers(somethingNoteworthy);
		}
	}
	
	public class ObjectThatIsObserving implements Observer {
		//implementation omitted
		
		public void update(Observable obs, Object somethingNoteworthy) { 
			if (somethingNoteworthy instanceof Foo) {
				doVeryInterestingStuffWith((Foo) somethingNoteworthy);
			}
		}

		private void doVeryInterestingStuffWith(Foo somethingNoteworthy) {
			//implementation omitted
		}
	}

	public class Main {
		public static void main(String[] args) {
	        ObjectBeingObserved initiator = new ObjectBeingObserved();
	        ObjectThatIsObserving responder = new ObjectThatIsObserving();

	        initiator.addObserver(responder);
	        initiator.doSomething(somethingNoteworthy);  
									// responder is triggered and
	       							// executes private method 
	       							// doVeryInterestingStuffWith
	    }
	}

	
```

An alternative example.
```java
	public class ObjectBeingObserved {
		//implementation omitted

		List<ImplementMe> listeners = new ArrayList<ImplementMe>();

		public void addListener(ImplementMe toAdd) {
        	listeners.add(toAdd);
    	}
		
		public void doSomething(Foo somethingNoteworthy) {
			for (ImplementMe listener : listeners) {
        		listener.doVeryInterestingStuffWith(somethingNoteworthy);
    		} 
		}

	}

	interface ImplementMe {
		void doVeryInterestingStuffWith(Foo somethingNoteworthy);
	}

	public class ObjectThatIsObserving implements ImplementMe {
		//implementation omitted

		public void doVeryInterestingStuffWith(Foo somethingNoteworthy) {
			doStuff(somethingNoteworthy);
		}

		private void doStuff(Foo somethingNoteworthy) {
			//implementation omitted
		}
	}

	public class Main {
		public static void main(String[] args) {
	        ObjectBeingObserved initiator = new ObjectBeingObserved();
	        ObjectThatIsObserving responder = new ObjectThatIsObserving();

	        initiator.addListener(responder);
	        initiator.doSomething(somethingNoteworthy);  
									// responder is triggered and
	        						// executes private method doStuff
	    }
	}
```
